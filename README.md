Setup based on: https://www.atlassian.com/git/tutorials/dotfiles
and modified for `pre-commit`  compatibility as seen
[here](https://github.com/pre-commit/pre-commit/issues/1657#issuecomment-715620323).
