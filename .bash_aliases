#!/bin/bash

# Configure dotfile management using git
# Usage:
#   dotfiles git status
#   dotfiles pre-commit run -a
alias dotfiles='GIT_DIR=$HOME/.configs/ GIT_WORK_TREE=$HOME'

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias sl='sl -4lewd'

alias tb='taskbook'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias dd='sudo dd status=progress'
alias dd-np='dd'

alias gcc="LANG=en gcc"

alias mount='sudo mount'
alias umount='sudo umount'

function update {
   IS_X11=1
   if [ -n "$WAYLAND_DISPLAY" ] || [ "$XDG_SESSION_TYPE" = "wayland" ]; then
      unset IS_X11
   fi
   # Parse Options
   while [[ $# -gt 0 ]]; do
      case $1 in
         -y | --yes | --noconfirm | --no-confirm)
            NO_CONFIRM=1
            shift
            ;;
         *)
            # unrecognized
            shift
            ;;
      esac
   done

   if command -v yay >/dev/null; then
      yay -Syu ${NO_CONFIRM:+--noconfirm}
   elif command -v pacman >/dev/null; then
      sudo pacman -Syu ${NO_CONFIRM:+--noconfirm}
   elif command -v apt >/dev/null; then
      sudo apt update && sudo apt upgrade ${NO_CONFIRM:+-y}
   fi

   command -v tlmgr >/dev/null && sudo tlmgr update --all
   command -v apm >/dev/null && apm update ${NO_CONFIRM:+--no-confirm}
   command -v gnome-shell-extension-installer >/dev/null \
      && if [ "$(gsettings get org.gnome.shell disable-user-extensions)" = false ]; then
         gnome-shell-extension-installer ${NO_CONFIRM:+--yes} \
            --update ${IS_X11:+--restart-shell}
      else
         echo Gnome Extensions are disabled and will not be updated
      fi
}

function usbrekt {
   if [[ $(id -u) -ne 0 ]]; then
      echo "Please run as root"
      exit 1
   fi
   while true; do
      read -r -p "This will make all contents of ${1} inaccessible. Continue? [y\n]" yn
      case $yn in
         [Yy]*)
            dd if=/dev/zero of="$1" bs=4096 count=1
            fdisk "$1"
            break
            ;;
         [Nn]*) exit ;;
         *) echo "Please answer yes or no." ;;
      esac
   done
}

function ddstat {
   if [[ $(id -u) -ne 0 ]]; then
      echo "Please run as root"
      exit 1
   fi
   dd if="$1" conv=noerror,notrunc,sync \
      | pv -s "$(stat -c %s "$1")" | dd of="$2"
}

if command -v thefuck >/dev/null; then
   eval "$(thefuck --alias)"
fi

function newlatex {
   cp ~/.latex_template.tex "$1".tex
}

function newabgabe {
   cp ~/.latex_template_abgabe.tex "$1".tex
}

function pdfcompress {
   gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dBATCH -dQUIET -sOutputFile="$2" "$1"
}
