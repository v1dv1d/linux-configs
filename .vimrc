" Zeilennummern
set number
set relativenumber
hi LineNr	ctermfg=102

set nocompatible
syntax on
filetype plugin indent on

" Zeilenbegrenzung
set colorcolumn=80
hi ColorColumn	ctermfg=000	ctermbg=102

" Indentation
set ts=4 sts=4 sw=4 noet
au filetype haskell setl ts=4 sts=4 sw=4 et
