#!/bin/bash

# \[ und \] sollten aus Kompatiblitaetsgruenden um
# Formatierungsescapesequences gesetzt werden.
# Für die Formatsttrings guckst du hier:
#		<https://wiki.ubuntuusers.de/Bash/Prompt>
export GIT_PS1_SHOWDIRTYSTATE=1

function prompt_command {
   local RESET='\[\033[0m\]'
   # shellcheck disable=SC2034
   local BOLD='\[\033[1m\]'

   local BLUE='\[\033[1;34m\]'
   local RED='\[\033[31m\]'
   local PINK='\[\033[1;35m\]'
   local GREEN='\[\033[1;32m\]'

   local PROMPT=""

   if [[ -n $SSH_CLIENT ]]; then
      PROMPT+="${GREEN}${HOSTNAME}"
   fi
   # Zeigt die aktuelle Git-Branch, außer die Repo liegt im home-Verzeichnis,
   # ist also die Repo die die config Dateien überwacht
   #BRANCH="${PINK}\$(git_ps1 '%s ')"
   PROMPT+="${PINK}\$(git_ps1 '%s ')"

   if [[ $(id -u) -eq 0 ]]; then
      #	IFROOT="${RED}#"
      PROMPT+="${RED}#"
   fi

   PROMPT+="${BLUE}\W "

   # shellcheck disable=SC2181 # not a useful warning in a prompt
   if [ $? -ne 0 ]; then
      #	ERRPROMPT="${RED}0_0 ($?) "
      PROMPT+="${RED}0_0 ($?) "
   fi

   #echo " ${BRANCH}${IFROOT}${BLUE}\W ${ERRPROMPT}${RESET}"
   echo "${BOLD}${PROMPT}${RESET}"
}
